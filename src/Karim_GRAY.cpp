#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "iostream"

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
Mat img;
Mat gray;
string chemin;
//cout << "Indiquez le chemin d'acces de l'image a traiter" << endl;
//cin >> chemin;

chemin = argv[1];

img = imread(chemin);
cvtColor(img,gray,CV_BGR2GRAY);//On peut remplacer HSV par YUV ou GRAY
//imshow("Original Image", gray);
imwrite("out.jpg", gray);
//waitKey();

return (0);
}
